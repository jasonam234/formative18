package com.jason.mahasiswa.mahasiswa;

public class Mahasiswa {
	private String namaMahasiswa;
	private String nimMahasiswa;
	private int umurMahasiswa;
	
	public Mahasiswa(String nama, String nim, int umur) {
		this.namaMahasiswa = nama;
		this.nimMahasiswa = nim;
		this.umurMahasiswa = umur;
	}
	
	public String getNamaMahasiswa() {
		return this.namaMahasiswa;
	}
	
	public String getNimMahasiswa() {
		return this.nimMahasiswa;
	}
	
	public int getUmurMahasiswa() {
		return this.umurMahasiswa;
	}
}
