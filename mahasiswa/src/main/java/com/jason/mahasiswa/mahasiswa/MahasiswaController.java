package com.jason.mahasiswa.mahasiswa;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class MahasiswaController {
	private DataMahasiswa dm;
	
	public MahasiswaController(DataMahasiswa dm) {
		super();
		this.dm = dm;
	}
	
	int addMahasiswa() {
		ArrayList<Mahasiswa> mahasiswa = dm.retrieveAllData();
		int ctr = 0;
		int lastMahasiswa = mahasiswa.size();
		
		if(mahasiswa.get(mahasiswa.size()-1).getNamaMahasiswa().equals(mahasiswa.get(mahasiswa.size()-1).getNamaMahasiswa())) {
			System.out.println("[ADD] Mahasiswa " + mahasiswa.get(mahasiswa.size()-1).getNamaMahasiswa() + " is Added");
			ctr = ctr + 1;
		}
		return ctr;
	}
	
	int deleteMahasiswa() {
		ArrayList<Mahasiswa> mahasiswa = dm.retrieveAllData();
		int ctr = 0;
		int initialSize = mahasiswa.size();
		
		mahasiswa.remove(0);
		
		if(mahasiswa.size() < initialSize) {
			System.out.println("[DEL] Mahasiswa deleted");
			ctr = ctr + 1;
		}
		return ctr;
	}
	
	int retrieveMahasiswa() {
		ArrayList<Mahasiswa> mahasiswa = dm.retrieveAllData();
		int ctr = 0;
		int dataSize = mahasiswa.size();
		
		if(dataSize == mahasiswa.size()) {
			System.out.println("[GET] Data succesfully retrieved");
			ctr = ctr + 1;
		}
		return ctr;
	}
	
	int checkMahasiswaNim() {
		ArrayList<Mahasiswa> mahasiswa = dm.retrieveAllData();
		int ctr = 0;
		
		for(int i = 0; i < mahasiswa.size(); i++) {
			if(Pattern.compile("^[NIM]*-[0-9]{5}$").matcher(mahasiswa.get(i).getNimMahasiswa()).matches()) {
				System.out.println("[NIM] NIM is Valid");
			}else {
				System.out.println("[NIM] Invalid NIM at data " + (i+1));
				ctr = ctr + 1;
			}
		}
		return ctr;
	}
	
	int checkMahasiswaAge() {
		ArrayList<Mahasiswa> mahasiswa = dm.retrieveAllData();
		int ctr = 0;
		
		for(int i = 0; i < mahasiswa.size(); i++) {
			if(mahasiswa.get(i).getUmurMahasiswa() < 17 || mahasiswa.get(i).getUmurMahasiswa() > 25) {
				System.out.println("[AGE] Invalid age at data " + (i+1));
				ctr = ctr + 1;
			}else {
				System.out.println("[AGE] Age is Valid");
			}
		}
		return ctr;
	}
	
	int checkMahasiswaName() {
		ArrayList<Mahasiswa> mahasiswa = dm.retrieveAllData();
		int ctr = 0;
		
		for(int i = 0; i < mahasiswa.size(); i++) {
			if(Pattern.compile(".*[0-9].*").matcher(mahasiswa.get(0).getNamaMahasiswa()).matches()) {
				System.out.println("[NAM] Invalid Name at data " + (i+1));
				ctr = ctr + 1;
			}else {
				System.out.println("[NAM] Name is Valid");
			}
		}
		return ctr;
	}
}
