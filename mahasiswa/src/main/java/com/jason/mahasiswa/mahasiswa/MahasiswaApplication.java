package com.jason.mahasiswa.mahasiswa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MahasiswaApplication {

	public static int main(String[] args) {
		SpringApplication.run(MahasiswaApplication.class, args);
		return 1;
	}

}
