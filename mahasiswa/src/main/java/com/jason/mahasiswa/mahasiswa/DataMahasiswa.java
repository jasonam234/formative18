package com.jason.mahasiswa.mahasiswa;

import java.util.ArrayList;

public interface DataMahasiswa {
	ArrayList<Mahasiswa> retrieveAllData();
}
