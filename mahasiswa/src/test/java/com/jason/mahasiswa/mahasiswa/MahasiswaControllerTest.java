package com.jason.mahasiswa.mahasiswa;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MahasiswaControllerTest {
	@Mock
	DataMahasiswa dataMahasiswa;
	
	@InjectMocks
	MahasiswaController mahasiswaDatabase;
	
	@Test
	public void addMahasiswaTestTrue() {
		Mahasiswa jason = new Mahasiswa("Jason", "NIM-23765", 22);
		Mahasiswa adrian = new Mahasiswa("Adrian", "NIM-11234", 23);
		Mahasiswa mahalim = new Mahasiswa("Mahalim", "NIM-99875", 24);
		ArrayList<Mahasiswa> mahasiswa= new ArrayList<Mahasiswa>();
		mahasiswa.add(jason);
		mahasiswa.add(adrian);
		mahasiswa.add(mahalim);
		when(dataMahasiswa.retrieveAllData()).thenReturn(mahasiswa);
		
		assertEquals(1, mahasiswaDatabase.addMahasiswa());
	}
	
	@Test
	public void deleteMahasiswaTest() {
		Mahasiswa jason = new Mahasiswa("Jason", "NIM-23765", 22);
		Mahasiswa adrian = new Mahasiswa("Adrian", "NIM-11234", 23);
		Mahasiswa mahalim = new Mahasiswa("Mahalim", "NIM-99875", 24);
		ArrayList<Mahasiswa> mahasiswa= new ArrayList<Mahasiswa>();
		mahasiswa.add(jason);
		mahasiswa.add(adrian);
		mahasiswa.add(mahalim);
		when(dataMahasiswa.retrieveAllData()).thenReturn(mahasiswa);
		assertEquals(1, mahasiswaDatabase.deleteMahasiswa());
	}
	
	@Test
	public void retrieveMahasiswaTestTrue() {
		Mahasiswa jason = new Mahasiswa("Jason", "NIM-23765", 22);
		Mahasiswa adrian = new Mahasiswa("Adrian", "NIM-11234", 23);
		Mahasiswa mahalim = new Mahasiswa("Mahalim", "NIM-99875", 24);
		ArrayList<Mahasiswa> mahasiswa= new ArrayList<Mahasiswa>();
		mahasiswa.add(jason);
		mahasiswa.add(adrian);
		mahasiswa.add(mahalim);
		when(dataMahasiswa.retrieveAllData()).thenReturn(mahasiswa);
		
		assertEquals(1, mahasiswaDatabase.retrieveMahasiswa());
	}
	
	@Test
	public void checkMahasiswaNimTrue() {
		Mahasiswa jason = new Mahasiswa("Jason", "NIM-23765", 22);
		Mahasiswa adrian = new Mahasiswa("Adrian", "NIM-11234", 23);
		Mahasiswa mahalim = new Mahasiswa("Mahalim", "NIM-99875", 24);
		ArrayList<Mahasiswa> mahasiswa= new ArrayList<Mahasiswa>();
		mahasiswa.add(jason);
		mahasiswa.add(adrian);
		mahasiswa.add(mahalim);
		when(dataMahasiswa.retrieveAllData()).thenReturn(mahasiswa);
		
		assertEquals(0, mahasiswaDatabase.checkMahasiswaNim());
	}
	
	@Test
	public void checkMahasiswaNimFalse() {
		Mahasiswa jason = new Mahasiswa("Jason", "NIM-2765", 22);
		Mahasiswa adrian = new Mahasiswa("Adrian", "NM-11234", 23);
		Mahasiswa mahalim = new Mahasiswa("Mahalim", "NIM-939875", 24);
		ArrayList<Mahasiswa> mahasiswa= new ArrayList<Mahasiswa>();
		mahasiswa.add(jason);
		mahasiswa.add(adrian);
		mahasiswa.add(mahalim);
		when(dataMahasiswa.retrieveAllData()).thenReturn(mahasiswa);
		
		assertTrue(mahasiswaDatabase.checkMahasiswaNim() > 0);
	}
	
	@Test
	public void checkMahasiswaAgeTrue() {
		Mahasiswa jason = new Mahasiswa("Jason", "NIM-23765", 22);
		Mahasiswa adrian = new Mahasiswa("Adrian", "NIM-11234", 23);
		Mahasiswa mahalim = new Mahasiswa("Mahalim", "NIM-99875", 24);
		ArrayList<Mahasiswa> mahasiswa= new ArrayList<Mahasiswa>();
		mahasiswa.add(jason);
		mahasiswa.add(adrian);
		mahasiswa.add(mahalim);
		when(dataMahasiswa.retrieveAllData()).thenReturn(mahasiswa);
		
		assertEquals(0, mahasiswaDatabase.checkMahasiswaAge());
	}
	
	@Test
	public void checkMahasiswaAgeFalse() {
		Mahasiswa jason = new Mahasiswa("Jason", "NIM-23765", 15);
		Mahasiswa adrian = new Mahasiswa("Adrian", "NIM-11234", 10);
		Mahasiswa mahalim = new Mahasiswa("Mahalim", "NIM-99875", 30);
		ArrayList<Mahasiswa> mahasiswa= new ArrayList<Mahasiswa>();
		mahasiswa.add(jason);
		mahasiswa.add(adrian);
		mahasiswa.add(mahalim);
		when(dataMahasiswa.retrieveAllData()).thenReturn(mahasiswa);
		
		assertTrue(mahasiswaDatabase.checkMahasiswaAge() > 0);
	}
	
	@Test
	public void checkMahasiswaNameTrue() {
		Mahasiswa jason = new Mahasiswa("Jason", "NIM-23765", 22);
		Mahasiswa adrian = new Mahasiswa("Adrian", "NIM-11234", 23);
		Mahasiswa mahalim = new Mahasiswa("Mahalim", "NIM-99875", 24);
		ArrayList<Mahasiswa> mahasiswa= new ArrayList<Mahasiswa>();
		mahasiswa.add(jason);
		mahasiswa.add(adrian);
		mahasiswa.add(mahalim);
		when(dataMahasiswa.retrieveAllData()).thenReturn(mahasiswa);
		
		assertEquals(0, mahasiswaDatabase.checkMahasiswaName());
	}
	
	@Test
	public void checkMahasiswaNameFalse() {
		Mahasiswa jason = new Mahasiswa("jasonam234", "NIM-23765", 22);
		Mahasiswa adrian = new Mahasiswa("999831", "NIM-11234", 23);
		Mahasiswa mahalim = new Mahasiswa("NIM-99847", "Mahalim", 24);
		ArrayList<Mahasiswa> mahasiswa= new ArrayList<Mahasiswa>();
		mahasiswa.add(jason);
		mahasiswa.add(adrian);
		mahasiswa.add(mahalim);
		when(dataMahasiswa.retrieveAllData()).thenReturn(mahasiswa);
		
		assertTrue(mahasiswaDatabase.checkMahasiswaName() > 0);
	}
	
	@Test
	public void checkMain() {
		String[] input = {"aaa"};
		assertEquals(1, MahasiswaApplication.main(input));
	}
}
